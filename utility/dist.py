from math import sin, cos, sqrt, atan2, radians

R = 6373.0  # радиус земли в километрах
def distance(x, y):
    """
    Параметры
    ----------
    x : tuple, широта и долгота первой геокоординаты
    y : tuple, широта и долгота второй геокоординаты

    Результат
    ----------
    result : дистанция в километрах между двумя геокоординатами
    """
    lat_a, long_a, lat_b, long_b = map(radians, [*x, *y])
    dlon = long_b - long_a
    dlat = lat_b - lat_a
    a = sin(dlat / 2) ** 2 + cos(lat_a) * cos(lat_b) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return R * c