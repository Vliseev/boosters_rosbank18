from threading import Thread

import requests

class UpdatedList(list):
    def __init__(self, num_update=0):
        super().__init__()
        self.num_update = num_update


def format_point(longitude, latitude):
    return '%0.7f,%0.7f' % (float(longitude), float(latitude),)


class OSMGeocod:
    API_ADDR = "https://nominatim.openstreetmap.org/reverse?"
    PARAMS = {'format': 'json'}

    @classmethod
    def request(cls, latitude: float, longitude: float) -> dict:
        """Requests passed address and returns content of `response` key.
        Raises `YandexGeocoderHttpException` if response's status code is
        different from `200`.
        """
        points = format_point(longitude, latitude)
        response = requests.get(cls.API_ADDR, params=dict(
            lat=latitude, lon=longitude, **cls.PARAMS))

        return response.json()


class OpenCage:
    """
    "https://api.opencagedata.com/geocode/v1/json?q=-22.6792%2C+14.5272&key=07ebcaa6ad624c7a9b7b2d835784fc9c&pretty=1"
    """
    API_ADDR = "https://api.opencagedata.com/geocode/v1/json?"

    # PARAMS = {'format': 'json'}

    @classmethod
    def request(cls, latitude: float, longitude: float, key: str) -> dict:
        """Requests passed address and returns content of `response` key.
        Raises `YandexGeocoderHttpException` if response's status code is
        different from `200`.
        """
        points = "q={0:f}%2C+{1:f}".format(latitude, longitude)
        params = "%s&key=%s&pretty=1" % (points, key)
        response = requests.get(cls.API_ADDR, params=params)

        if not response.ok:
            raise Exception('Error responce')
        return response.json()


class YandexGeocode:
    API_ADDR = "https://geocode-maps.yandex.ru/1.x/?"
    PARAMS = {'format': 'json'}

    @classmethod
    def request(cls, latitude: float, longitude: float) -> dict:
        """Requests passed address and returns content of `response` key.
        Raises `YandexGeocoderHttpException` if response's status code is
        different from `200`.
        """
        points = format_point(longitude, latitude)
        response = requests.get(cls.API_ADDR, params=dict(
            geocode=points, **cls.PARAMS))

        return response.json()

    @classmethod
    def get_coord(cls, s: str):
        addr = "+".join(s.split())
        response = requests.get(cls.API_ADDR, params=dict(
            geocode=addr, **cls.PARAMS))

        result = \
        response.json()

        return result


class DownloadThread(Thread):
    """
    Пример скачивание файла используя многопоточность
    """

    def __init__(self, latitude: float, longitude: float):
        """Инициализация потока"""
        Thread.__init__(self)
        self.lat = latitude
        self.long = longitude

    def run(self):
        """Запуск потока"""
        return OSMGeocod.request(self.lat, self.long)
