import pickle as pkl
from itertools import chain

import numpy as np
import pandas as pd


def parse_results_openc(p_to_parsef, df):
    with open(p_to_parsef, "rb") as f:
        result = pkl.load(f)
    old_idx = df.index
    df.index = np.arange(0, df.shape[0])

    road = pd.Series([el['results'][0]['components']['road']
                      if 'road' in el['results'][0]['components'] else None
                      for el in result])
    _type = pd.Series([el['results'][0]['components']['_type']
                       if '_type' in el['results'][0]['components'] else None
                       for el in result])

    df['road'] = road
    df['type'] = _type
    df.index = old_idx

    return df


def parse_results_yandex(p_to_parsef, df):
    def get_kind(line):
        return line['kind']

    def get_locality(resp):
        geo_obj = resp['response']['GeoObjectCollection'] \
            ['featureMember'][0]['GeoObject']['metaDataProperty'] \
            ['GeocoderMetaData']['Address']['Components']
        locality = next((x for x in geo_obj if get_kind(x) == 'locality'), None)

        if locality:
            locality = locality['name']

        return locality

    def get_province(resp):
        geo_obj = resp['response']['GeoObjectCollection'] \
            ['featureMember'][0]['GeoObject']['metaDataProperty'] \
            ['GeocoderMetaData']['Address']['Components']
        province = next((x for x in geo_obj if get_kind(x) == 'province'), None)
        if province:
            province = province['name']

        return province

    def get_country(resp):
        geo_obj = resp['response']['GeoObjectCollection'] \
            ['featureMember'][0]['GeoObject']['metaDataProperty'] \
            ['GeocoderMetaData']['Address']['Components']
        country = next((x for x in geo_obj if get_kind(x) == 'country'), None)
        if country:
            country = country['name']

        return country

    with open(p_to_parsef, "rb") as f:
        result = pkl.load(f)
    old_idx = df.index
    df.index = np.arange(0, df.shape[0])

    locality = pd.Series(get_locality(el) for el in result)
    province = pd.Series(get_province(el) for el in result)
    country = pd.Series(get_country(el) for el in result)

    df['locality'] = locality
    df['province'] = province
    df['country'] = country
    df.index = old_idx

    return df


def train_fill_get_coord():
    def get_coord(x):
        coord = x['response']['GeoObjectCollection'] \
            ['featureMember']
        coord = coord[0]['GeoObject']['Point']['pos'].split() if len(
            coord) > 0 else None
        return tuple(map(float, coord)) if coord else (None, None)

    train = pd.read_csv('train.csv', index_col=0)
    train_fill = train[train.lat.isna()]
    with open("data/location/train_fill_coord.pkl", "rb") as f:
        result = pkl.load(f)

    coord = [get_coord(el) for el in result]
    coord = list(chain.from_iterable(coord))
    lat = coord[1::2]
    long = coord[::2]
    train_fill['lat'] = lat
    train_fill['long'] = long
    train_fill = train_fill[~train_fill.lat.isna()]
    train_fill.to_csv("data/data_frames/fill_train.csv")


def test_fill_add_coord():
    def get_coord(x):
        coord = x['response']['GeoObjectCollection'] \
            ['featureMember']
        coord = coord[0]['GeoObject']['Point']['pos'].split() if len(
            coord) > 0 else None
        return tuple(map(float, coord)) if coord else (None, None)

    test = pd.read_csv('test.csv', index_col=0)
    test_fill = test[test.lat.isna()]
    with open("data/location/parsed_test_fill_coord", "rb") as f:
        result = pkl.load(f)

    coord = [get_coord(el) for el in result]
    coord = list(chain.from_iterable(coord))
    lat = coord[1::2]
    long = coord[::2]
    test_fill['lat'] = lat
    test_fill['long'] = long
    test_fill = test_fill[~test_fill.lat.isna()]
    test_fill.to_csv("data/data_frames/fill_test.csv")


def add_type_and_roads_train():
    train = pd.read_csv("train.csv", index_col=0)
    train = train.dropna()
    df_with_component = parse_results_openc("data/location/parsed_result_openc",
                                            train)
    df_with_component = parse_results_yandex(
        "data/location/train_parsed_result", df_with_component)
    df_with_component.to_csv("data/data_frames/train_fe_group.csv")


def add_type_and_roads_train_fill():
    train = pd.read_csv("data/data_frames/fill_train.csv", index_col=0)
    df_with_component = parse_results_openc(
        "data/location/parsed_result_fill_openc", train)
    df_with_component = parse_results_yandex(
        "data/location/train_ya_parse_fill", df_with_component)
    df_with_component.to_csv("data/data_frames/train_fill_fe_group.csv")


def add_type_and_roads_test():
    test = pd.read_csv("data/data_frames/test_with_na_coord.csv", index_col=0)
    df_with_component = parse_results_openc(
        "data/location/parsed_result_test_openc", test)
    df_with_component = parse_results_yandex(
        "data/location/test_with_na_coord_ya", df_with_component)
    df_with_component.to_csv("data/data_frames/test_group_loc.csv")


if __name__ == '__main__':
    #
    # for line in g[:100]:
    #     lst = line['response']['GeoObjectCollection']['featureMember']
    #     get_kind = lambda x: \
    #     x['GeoObject']['metaDataProperty']['GeocoderMetaData']['kind']
    #     locality = \
    #     next((x for x in lst if get_kind(x) == 'locality'), None)['GeoObject'][
    #         'metaDataProperty']['GeocoderMetaData']['text']
    #     province = \
    #     next((x for x in lst if get_kind(x) == 'province'), None)['GeoObject'][
    #         'metaDataProperty']['GeocoderMetaData']['text']
    #     print(locality, "\t", province)
    # r = YandexGeocode.get_coord("Россия, Сахалинская область")
    # print(r)
    add_type_and_roads_test()

