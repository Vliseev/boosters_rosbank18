import abc
import pickle as pkl
from itertools import chain
from pathlib import Path

import pandas as pd
import tqdm
from joblib import delayed, Parallel

from utility.parsers import YandexGeocode, OpenCage


class Parser:
    """
    Define the interface of interest to clients.
    Maintain a reference to a Strategy object.
    """

    def __init__(self, strategy):
        self._strategy = strategy

    def parse(self, df, path_to_save, *args, **kwargs):
        start = 0
        result = []
        parallel = Parallel(4, backend="threading", verbose=10)

        my_file = Path(path_to_save)
        if my_file.is_file():
            with open(path_to_save, "rb") as f:
                result = pkl.load(f)
            start = len(result)

        for index in tqdm.tqdm(range(start, df.shape[0], 60)):
            rows = df.iloc[index:index + 60]
            parse_res = self._strategy.parse(parallel, rows, *args, **kwargs)
            # parallel(
            # delayed(YandexGeocode.request)(row['lat'], row['long']) for
            # index, row in rows.iterrows())
            result.extend(parse_res)
            with open(path_to_save, "wb") as f:
                pkl.dump(result, f)


class Strategy(metaclass=abc.ABCMeta):
    """
    Declare an interface common to all supported algorithms. Context
    uses this interface to call the algorithm defined by a
    ConcreteStrategy.
    """

    @abc.abstractmethod
    def parse(self, *args, **kwargs):
        pass


class YaParseLoc(Strategy):
    """
    Implement the algorithm using the Strategy interface.
    """

    def parse(self, parallel, rows):
        return parallel(
            delayed(YandexGeocode.request)(row['lat'], row['long']) for
            index, row in rows.iterrows())


class YaParseCoord(Strategy):
    def parse(self, parallel, rows):
        return parallel(
            delayed(YandexGeocode.get_coord)(row['address']) for
            index, row in rows.iterrows())


class OpenCageParse(Strategy):
    def parse(self, parallel, rows, api_key: str):
        return parallel(
            delayed(OpenCage.request)(row['lat'], row['long'],
                                      api_key) for
            index, row in rows.iterrows())


def pars_openc_train():
    train = pd.read_csv('train.csv', index_col=0)
    clear_train = train.dropna()
    parser = Parser(OpenCageParse())

    with open("api_token", "r") as f:
        token = f.readline()

    parser.parse(clear_train, "data/location/parsed_result_openc", token)


def train_ya_parse_fill():
    train = pd.read_csv('train.csv', index_col=0)
    parser = Parser(YaParseLoc())
    parser.parse(train, "data/location/train_parsed_result")


def train_ya_parse():
    train = pd.read_csv('data/data_frames/fill_train.csv', index_col=0)
    clear_train = train.dropna()
    parser = Parser(YaParseLoc())
    parser.parse(train, "data/location/train_parsed_result")


def open_cage_parse_fill_loc_train():
    train = pd.read_csv('data/data_frames/fill_train.csv', index_col=0)
    train = train[~train.lat.isna()]
    parser = Parser(OpenCageParse())

    with open("api_token", "r") as f:
        token = f.readline()

    parser.parse(train, "data/location/parsed_result_fill_openc", token)


def get_coord_fill_test():
    test = pd.read_csv("test.csv", index_col=0)
    test_fill = test[test.lat.isna()]
    parser = Parser(YaParseCoord())
    parser.parse(test_fill, "data/location/parsed_test_fill_coord")


def parce_openc_test():
    test = pd.read_csv('data/data_frames/test_with_na_coord.csv', index_col=0)
    clear_test = test[~test.lat.isna()]
    parser = Parser(OpenCageParse())
    # parse_coord(clear_train, "data/location/parsed_reszult")
    with open("api_token", "r") as f:
        token = f.readline()
    parser.parse(clear_test, "data/location/parsed_result_test_openc",
                 token)


def test_ya_parse():
    train = pd.read_csv('data/data_frames/test_with_na_coord.csv', index_col=0)
    clear_train = train.dropna()
    parser = Parser(YaParseLoc())
    parser.parse(train, "data/location/test_with_na_coord_ya")


def main():
    # open_cage_parse_fill_loc()
    test_ya_parse()


if __name__ == '__main__':
    main()
